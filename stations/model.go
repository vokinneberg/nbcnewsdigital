package stations

const (
	InService    = 1
	NotInService = 3
)

type Station struct {
	Id             int    `json:"id"`
	Name           string `json:"stationName"`
	Address        string `json:"stAddress1"`
	BikesAvailable int    `json:"availableBikes"`
	TotalDocks     int    `json:"totalDocks"`
	StatusKey      int    `json:"statusKey"`
}
