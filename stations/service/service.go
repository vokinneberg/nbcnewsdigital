package service

import (
	"NBCNewsDigital/stations"
	"NBCNewsDigital/stations/repository"
	"fmt"
	"sort"
	"strings"
)

type StationService interface {
	One(id int) (*stations.Station, error)
	All(page int) ([]stations.Station, error)
	InService(page int) ([]stations.Station, error)
	NotInService(page int) ([]stations.Station, error)
	Search(pattern string) ([]stations.Station, error)
}

type stationService struct {
	repo repository.StationRepository
}

func NewStationsService(r repository.StationRepository) StationService {
	return &stationService{repo: r}
}

func (s *stationService) One(id int) (*stations.Station, error) {
	allStations, err := s.repo.Stations()
	if err != nil {
		return nil, err
	}

	sort.Slice(allStations, func(i int, j int) bool {
		return allStations[i].Id < allStations[j].Id
	})

	i := sort.Search(len(allStations), func(i int) bool {
		return allStations[i].Id >= id
	})

	if i == len(allStations) {
		return nil, fmt.Errorf("station with id #{id} not found")
	} else {
		return &allStations[i], nil
	}
}

func (s *stationService) All(page int) ([]stations.Station, error) {
	allStations, err := s.repo.Stations()
	if err != nil {
		return nil, err
	}

	return paging(allStations, page), nil
}

func (s *stationService) InService(page int) ([]stations.Station, error) {
	allStations, err := s.repo.Stations()
	if err != nil {
		return nil, err
	}

	inServiceStations := filter(allStations, func(s stations.Station) bool {
		return s.StatusKey == stations.InService
	})

	return paging(inServiceStations, page), nil
}

func (s *stationService) NotInService(page int) ([]stations.Station, error) {
	allStations, err := s.repo.Stations()
	if err != nil {
		return nil, err
	}

	notInServiceStations := filter(allStations, func(s stations.Station) bool {
		return s.StatusKey == stations.NotInService
	})

	return paging(notInServiceStations, page), nil
}

func (s *stationService) Search(pattern string) ([]stations.Station, error) {
	allStations, err := s.repo.Stations()
	if err != nil {
		return nil, err
	}

	return filter(allStations, func(s stations.Station) bool {
		return strings.Contains(strings.ToLower(s.Name), strings.ToLower(pattern)) ||
			strings.Contains(strings.ToLower(s.Address), strings.ToLower(pattern))
	}), nil
}

func filter(s []stations.Station, test func(stations.Station) bool) (r []stations.Station) {
	for _, s := range s {
		if test(s) {
			r = append(r, s)
		}
	}
	return
}

func paging(s []stations.Station, page int) (r []stations.Station) {
	if page > 0 {
		offset := 20 * (page - 1)
		limit := 20 * page
		if offset > len(s) {
			return []stations.Station{}
		} else if limit > len(s) {
			return s[offset:]
		} else {
			return s[offset:limit]
		}
	} else {
		return s
	}
}
