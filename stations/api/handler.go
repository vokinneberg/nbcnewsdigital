package api

import (
	"NBCNewsDigital/common"
	"NBCNewsDigital/stations/service"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"net/http"
	"strconv"
	"strings"
)

type StationHandler interface {
	Stations(w http.ResponseWriter, r *http.Request)
	InServiceStations(w http.ResponseWriter, r *http.Request)
	NotInServiceStations(w http.ResponseWriter, r *http.Request)
	Search(w http.ResponseWriter, r *http.Request)
	Dockable(w http.ResponseWriter, r *http.Request)
}

type stationHandler struct {
	svc service.StationService
	log *logrus.Logger
}

func NewStationHandler(svc service.StationService, log *logrus.Logger) StationHandler {
	return &stationHandler{
		svc: svc,
		log: log,
	}
}

func (h *stationHandler) Stations(w http.ResponseWriter, r *http.Request) {
	var page int
	var err error
	if len(r.URL.Query().Get("page")) > 0 {
		page, err = strconv.Atoi(r.URL.Query().Get("page"))
		if err != nil {
			h.setError(w, r, common.ErrInvalidParameter, err)
			return
		}
	}

	result, err := h.svc.All(page)
	if err != nil {
		h.setError(w, r, common.ErrNotFound, err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(result)
	if err != nil {
		h.setError(w, r, common.ErrInternal, err)
		return
	}
}

func (h *stationHandler) InServiceStations(w http.ResponseWriter, r *http.Request) {
	var page int
	var err error
	if len(r.URL.Query().Get("page")) > 0 {
		page, err = strconv.Atoi(r.URL.Query().Get("page"))
		if err != nil {
			h.setError(w, r, common.ErrInvalidParameter, err)
			return
		}
	}

	result, err := h.svc.InService(page)
	if err != nil {
		h.setError(w, r, common.ErrNotFound, err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(result)
	if err != nil {
		h.setError(w, r, common.ErrInternal, err)
		return
	}
}

func (h *stationHandler) NotInServiceStations(w http.ResponseWriter, r *http.Request) {
	var page int
	var err error
	if len(r.URL.Query().Get("page")) > 0 {
		page, err = strconv.Atoi(r.URL.Query().Get("page"))
		if err != nil {
			h.setError(w, r, common.ErrInvalidParameter, err)
			return
		}
	}

	result, err := h.svc.NotInService(page)
	if err != nil {
		h.setError(w, r, common.ErrNotFound, err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(result)
	if err != nil {
		h.setError(w, r, common.ErrInternal, err)
		return
	}
}

func (h *stationHandler) Search(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	pattern := vars["searchstring"]

	result, err := h.svc.Search(pattern)
	if err != nil {
		h.setError(w, r, common.ErrInternal, err)
		return
	}

	if len(result) == 0 {
		h.setError(w, r, common.ErrNotFound, fmt.Errorf("no stations found by pattern #{searchstring}"))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(result)
	if err != nil {
		h.setError(w, r, common.ErrInternal, err)
		return
	}
}

func (h *stationHandler) Dockable(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	stationId, err := strconv.Atoi(vars["stationid"])
	if err != nil {
		h.setError(w, r, common.ErrInvalidParameter, err)
		return
	}
	bikesToReturn, err := strconv.Atoi(vars["bikestoreturn"])
	if err != nil {
		h.setError(w, r, common.ErrInvalidParameter, err)
		return
	}

	station, err := h.svc.One(stationId)
	if err != nil {
		h.setError(w, r, common.ErrNotFound, err)
		return
	}

	var result bool
	var message string
	if station.TotalDocks >= bikesToReturn {
		result = true
		message = "There are enough docks on the station."
	} else {
		result = false
		message = "Not enough docks on the station."
	}

	resp := struct {
		Dockable bool   `json:"dockable"`
		Message  string `json:"message"`
	}{
		Dockable: result,
		Message:  message,
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(resp)
	if err != nil {
		h.setError(w, r, common.ErrInternal, err)
		return
	}
}

func (h *stationHandler) setError(w http.ResponseWriter, r *http.Request, code common.ErrorCode, err error) {
	arguments := make(map[string]string)
	for k, v := range r.URL.Query() {
		arguments[k] = strings.Join(v, ",")
	}
	for k, v := range mux.Vars(r) {
		arguments[k] = v
	}

	params := common.HttpErrorParams{Path: r.URL.String(), Arguments: arguments}
	logEntry := h.log.WithField("request_parameters", params)
	logEntry.WithField("method", r.Method)

	if err != nil {
		logEntry.Error(err)
	}

	error, ok := common.Errors[code]
	if !ok {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(error.HTTPStatus)
	jsonResponse := &common.JsonHttpResponse{
		Message: error.Message,
	}
	if err := jsonResponse.WriteResponse(w); err != nil {
		h.log.Error(err)
		panic(err)
	}
}
