package repository

import "NBCNewsDigital/stations"

type StationRepository interface {
	Stations() ([]stations.Station, error)
}
