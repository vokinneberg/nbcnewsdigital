package repository

import (
	"NBCNewsDigital/stations"
	"encoding/json"
	"net/http"
)

type httpStationRepository struct {
	DataSource string
}

func NewHttpStationRepository(dataSource string) StationRepository {
	return &httpStationRepository{
		DataSource: dataSource,
	}
}

func (r *httpStationRepository) Stations() ([]stations.Station, error) {
	resp, err := http.Get(r.DataSource)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var data struct {
		StationList []stations.Station `json:"stationBeanList"`
	}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return nil, err
	}

	return data.StationList, nil
}
