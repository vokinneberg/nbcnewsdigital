# NBCNewsDigital coding test

Features:

* JSON API that returns data from an external data source.

Package structure:

* `bin` - contains application binary
* `common` - contains crosscutting components
* `config` - contains application configuration
* `handler` - contains implementation and tests of application HTTP handler
* `middleware` - contains implementation of application middleware
* `stations` - contains code related to stations entity and implementation of all aspects related to this entity
* `vendor` - vernor folder contains all application dependencies
* `main.go` - application entry point
* `main_test.go` - application entry point tests
* `go.mod` - go modules configuration
* `Makefile` - make utility configuration
* `README.md` - this file

## Configuration

Application configured with environment variables. It is easy to extend to environment variables though.

List of NBCNewsDigital settings with default values:

```go
    NND_SERVICE_NAME    - Application service name. No default value provided. Recommended to set it to `nbc-news-digital`.
    NND_SERVICE_HOST    - Application host. Default value set to `0.0.0.0`.
    NND_SERVICE_PORT    - Port server is listening on. The default value is `4000`.
    NND_DATA_SOURCE     - Stations data source. Use `https://feeds.citibikenyc.com/stations/stations.json`.
    NND_LOG_LEVEL       - Application logging level. Default logging level in `Info`.
```

## Getting Up and Running

### Package manager

Go modules `go mod` with vendoring mode used to set up dependencies for the project. Sources provided with vendor folder, so no need to download packages. Sources might be compiled right after download.

### Linters

I use [golanci-lint](https://golangci.com/ ) to statically check source code. If you're on Mac `golangci-lint` utility might be easily installed with with `brew`:

```shell
brew install golangci/tap/golangci-lint
```

For another platforms instructions could be found on the [linter official page]( https://golangci.com/).

Linter could be run against all sources in the folder recursively with the following command:

```shell
make lint
```

Considering that linting tool could not be installed on target computer this option is not part of building process.

### Test

Run following command to run application unit tests:

```shell
make test
```

### Build

Run following command to build application binary file:

```shell
make build
```

Or full build process including clean up and unit tests:

```shell
make
```

After build performed you can find resulting application binary file in the `bin` folder. Default binary name is `nbc-news-digital`.

### Run

Application could be run right from binary folder with the command:

```shell
./bin/nbc-news-digital
```

To make application running correctly export environment variables either with export command from your terminal or with the command `source .env`.


## Technical details

### REST Api

The application implemented as an HTTP server using `negroni` middleware with `gorilla mux` request router and dispatcher. The application has few abstract layers to make it more modular to make it easier to extend and test. 

* Repository - abstract layer that provides access to the data source. We could use various data sources such as HTTP, file, database and so on.
* Service - abstract layer that encapsulates all data manipulation business logic.
* Handler - contains actions that handle HTTP requests. It is close to the `controller` layer in terms of MVC pattern.
* Model - object model that represents entity properties. Used to transfer data from the data source to service and from service ack to the client.

### Efficiency!

To increase application efficiency, I've designed a caching middleware layer. It is implemented as a classic `negroni` middleware that could be integrated into any `negroni` application. I've used `golang-lru` 
data structure implementation from [Hashicorp](https://github.com/hashicorp/golang-lru) and implementation is very simple and only cache first-time fetched data and return cached data for other requests.
Requests are identified by its query string then `int64` hash key generated for every request to identify it. Request Body currently not used for a hash generation as the application does not provide `POST` HTTP
endpoints. It could be easily extended for any HTTP verb though. Cache TTL also not provided. Cached entry lifetime managed by LRU internal algorithm and cache size.

