# github.com/gorilla/mux v1.7.4
github.com/gorilla/mux
# github.com/hashicorp/golang-lru v0.5.4
github.com/hashicorp/golang-lru
github.com/hashicorp/golang-lru/simplelru
# github.com/konsorten/go-windows-terminal-sequences v1.0.1
github.com/konsorten/go-windows-terminal-sequences
# github.com/meatballhat/negroni-logrus v1.1.0
github.com/meatballhat/negroni-logrus
# github.com/sirupsen/logrus v1.4.2
github.com/sirupsen/logrus
# github.com/urfave/negroni v1.0.0
github.com/urfave/negroni
# golang.org/x/sys v0.0.0-20190422165155-953cdadca894
golang.org/x/sys/unix
