package main

import (
	"NBCNewsDigital/config"
	handler2 "NBCNewsDigital/handler"
	"NBCNewsDigital/stations/api"
	"NBCNewsDigital/stations/repository"
	"NBCNewsDigital/stations/service"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	// Get application config.
	conf := config.ReadConfig()

	// Init logger.
	log := logrus.New()
	log.Out = os.Stdout
	log.SetFormatter(&logrus.JSONFormatter{})
	log.SetLevel(conf.LogLevel)

	// Init dependencies.
	stationRepo := repository.NewHttpStationRepository(conf.DataSource)
	stationService := service.NewStationsService(stationRepo)
	stationHandler := api.NewStationHandler(stationService, log)

	// Init http routes.
	handler := handler2.NewHttpHandler(stationHandler, conf, log).Routes()

	// Init server.
	srv := &http.Server{
		Addr:         fmt.Sprintf("%s:%d", conf.ServiceHost, conf.ServicePort),
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
		IdleTimeout:  60 * time.Second,
		Handler:      handler,
	}

	// Graceful shutdown
	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)

	<-c

	ctx, cancel := context.WithTimeout(context.Background(), 100*time.Microsecond)
	defer cancel()

	if err := srv.Shutdown(ctx); err != nil {
		log.Error(err)
	}
	log.Println("shutting down server")
	os.Exit(0)
}
