package config

import (
	"github.com/sirupsen/logrus"
	"os"
	"strconv"
)

type Config struct {
	ServiceName string
	ServiceHost string
	ServicePort int
	DataSource  string
	LogLevel    logrus.Level
}

func ReadConfig() *Config {
	c := &Config{
		ServiceName: os.Getenv("NND_SERVICE_NAME"),
		ServiceHost: os.Getenv("NND_SERVICE_HOST"),
		DataSource:  os.Getenv("NND_DATA_SOURCE"),
	}

	servicePort, err := strconv.Atoi(os.Getenv("NND_SERVICE_PORT"))
	if err != nil {
		servicePort = 4000
	}
	c.ServicePort = servicePort

	logLevel, err := logrus.ParseLevel(os.Getenv("NND_LOG_LEVEL"))
	if err != nil {
		logLevel = logrus.InfoLevel
	}
	c.LogLevel = logLevel

	return c
}
