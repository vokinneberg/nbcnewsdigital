package common

import "fmt"

type Error struct {
	Message    string
	HTTPStatus int
}

type HttpErrorParams struct {
	Path      string            `json:"path,omitempty"`
	Arguments map[string]string `json:"arguments,omitempty"`
}

type ErrorCode int

const (
	ErrServiceTempUnavailable ErrorCode = iota
	ErrInternal
	ErrEmptyRequest
	ErrIncorrectRequest
	ErrInvalidParameter
	ErrWrongId
	ErrNotFound
)

var Errors = map[ErrorCode]Error{
	ErrServiceTempUnavailable: {Message: "service temporary unavailable", HTTPStatus: 503},
	ErrInternal:               {Message: "internal error", HTTPStatus: 500},
	ErrEmptyRequest:           {Message: "empty request", HTTPStatus: 400},
	ErrIncorrectRequest:       {Message: "incorrect request", HTTPStatus: 400},
	ErrInvalidParameter:       {Message: "invalid parameter", HTTPStatus: 400},
	ErrWrongId:                {Message: "wrong id", HTTPStatus: 400},
	ErrNotFound:               {Message: "not found", HTTPStatus: 404},
}

func (e Error) Error() string {
	return e.String()
}

func (e Error) String() string {
	return fmt.Sprintf("#{e.Message}")
}
