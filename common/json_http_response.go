package common

import (
	"encoding/json"
	"net/http"
)

type JsonHttpResponse struct {
	Message string      `json:"message,omitempty"`
	Data    interface{} `json:"data,omitempty"`
}

func (r *JsonHttpResponse) WriteResponse(w http.ResponseWriter) error {
	w.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(w).Encode(r)
	return err
}
