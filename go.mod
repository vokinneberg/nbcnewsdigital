module NBCNewsDigital

require github.com/gorilla/mux v1.7.4

require github.com/urfave/negroni v1.0.0

require github.com/meatballhat/negroni-logrus v1.1.0

require github.com/sirupsen/logrus v1.4.2

require (
	github.com/hashicorp/golang-lru v0.5.4
	github.com/stretchr/testify v1.2.2
)

go 1.13
