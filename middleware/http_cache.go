package middleware

import (
	"github.com/hashicorp/golang-lru"
	"github.com/urfave/negroni"
	"hash/fnv"
	"net/http"
	"net/http/httptest"
	"net/url"
	"sort"
	"strings"
)

const (
	HeaderContentType          = "Content-Type"
	ContentTypeApplicationJson = "application/json"
)

type HttpResponseEntry struct {
	Status int
	Header http.Header
	Body   []byte
}

type HttpCache struct {
	Cache *lru.Cache
}

func NewHttpCache(size int) *HttpCache {
	c, err := lru.New(size)
	if err != nil {
		panic("unable to initialize http cache")
	}
	return &HttpCache{Cache: c}
}

func (c *HttpCache) ServeHTTP(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	sortURLParams(r.URL)
	key := generateKey(r.URL.String())
	if c.Cache.Contains(key) {
		if respEntry, ok := c.Cache.Get(key); ok {
			resp := respEntry.(HttpResponseEntry)
			for k, v := range resp.Header {
				rw.Header().Set(k, strings.Join(v, ";"))
			}
			rw.WriteHeader(resp.Status)
			_, err := rw.Write(resp.Body)
			if err != nil {
				panic(err)
			}
			return
		}
	}

	w := httptest.NewRecorder()
	crw := newCacheResponseWriter(rw.(negroni.ResponseWriter), w)

	next(crw, r)

	c.Cache.Add(key, HttpResponseEntry{Status: w.Code, Header: w.HeaderMap, Body: w.Body.Bytes()})

	rw.WriteHeader(w.Code)
	_, err := rw.Write(w.Body.Bytes())
	if err != nil {
		panic(err)
	}
}

type cacheResponseWriter struct {
	w *httptest.ResponseRecorder
	negroni.ResponseWriter
	wroteHeader bool
}

func (crw *cacheResponseWriter) WriteHeader(code int) {
	if crw.w == nil {
		crw.ResponseWriter.WriteHeader(code)
	} else {
		crw.w.WriteHeader(code)
	}
	crw.wroteHeader = true
}

func (crw *cacheResponseWriter) Write(b []byte) (int, error) {
	if !crw.wroteHeader {
		if crw.w == nil {
			crw.ResponseWriter.WriteHeader(http.StatusOK)
		} else {
			crw.w.WriteHeader(http.StatusOK)
		}
	}

	if crw.w == nil {
		if len(crw.Header().Get(HeaderContentType)) == 0 {
			crw.Header().Set(HeaderContentType, ContentTypeApplicationJson)
		}
		return crw.ResponseWriter.Write(b)
	}

	if len(crw.w.Header().Get(HeaderContentType)) == 0 {
		crw.w.Header().Set(HeaderContentType, ContentTypeApplicationJson)
	}
	return crw.w.Write(b)
}

type cacheResponseWriterCloseNotifier struct {
	*cacheResponseWriter
}

func (rw *cacheResponseWriterCloseNotifier) CloseNotify() <-chan bool {
	return rw.ResponseWriter.(http.CloseNotifier).CloseNotify()
}

func newCacheResponseWriter(rw negroni.ResponseWriter, w *httptest.ResponseRecorder) negroni.ResponseWriter {
	wr := &cacheResponseWriter{w: w, ResponseWriter: rw}

	if _, ok := rw.(http.CloseNotifier); ok {
		return &cacheResponseWriterCloseNotifier{cacheResponseWriter: wr}
	}

	return wr
}

func generateKey(URL string) uint64 {
	hash := fnv.New64a()
	_, err := hash.Write([]byte(URL))
	if err != nil {
		panic(err)
	}

	return hash.Sum64()
}

func sortURLParams(URL *url.URL) {
	params := URL.Query()
	for _, param := range params {
		sort.Slice(param, func(i, j int) bool {
			return param[i] < param[j]
		})
	}
	URL.RawQuery = params.Encode()
}
