package handler

import (
	"NBCNewsDigital/common"
	"NBCNewsDigital/config"
	"NBCNewsDigital/middleware"
	"NBCNewsDigital/stations/api"
	"github.com/gorilla/mux"
	negronilogrus "github.com/meatballhat/negroni-logrus"
	"github.com/sirupsen/logrus"
	"github.com/urfave/negroni"
	"net/http"
	"strings"
)

const (
	RequestCacheSize = 256
)

type HttpHandler interface {
	Routes() http.Handler
}

type httpHandler struct {
	stationHandler api.StationHandler
	conf           *config.Config
	log            *logrus.Logger
}

func NewHttpHandler(stationHandler api.StationHandler, conf *config.Config, log *logrus.Logger) HttpHandler {
	return &httpHandler{
		stationHandler: stationHandler,
		conf:           conf,
		log:            log,
	}
}

func (h *httpHandler) Routes() http.Handler {
	// Set up API routes.
	router := mux.NewRouter()

	router.HandleFunc("/health", func(writer http.ResponseWriter, request *http.Request) {
		writer.WriteHeader(http.StatusOK)
	})

	router.HandleFunc("/stations", h.stationHandler.Stations).
		Methods(http.MethodGet)
	router.HandleFunc("/stations", h.stationHandler.Stations).
		Queries("page", "{page:[0-9]+}").
		Methods(http.MethodGet)

	router.HandleFunc("/stations/in-service", h.stationHandler.InServiceStations).
		Methods(http.MethodGet)
	router.HandleFunc("/stations/in-service", h.stationHandler.InServiceStations).
		Queries("page", "{page:[0-9]+}").
		Methods(http.MethodGet)

	router.HandleFunc("/stations/not-in-service", h.stationHandler.NotInServiceStations).
		Methods(http.MethodGet)
	router.HandleFunc("/stations/not-in-service", h.stationHandler.NotInServiceStations).
		Queries("page", "{page:[0-9]+}").
		Methods(http.MethodGet)

	router.HandleFunc("/stations/{searchstring}", h.stationHandler.Search).
		Methods(http.MethodGet)

	router.HandleFunc("/dockable/{stationid}/{bikestoreturn}", h.stationHandler.Dockable).
		Methods(http.MethodGet)

	logger := negronilogrus.NewMiddlewareFromLogger(h.log, h.conf.ServiceName)
	logger.Before = func(entry *logrus.Entry, req *http.Request, s string) *logrus.Entry {
		arguments := make(map[string]string)
		for k, v := range req.URL.Query() {
			arguments[k] = strings.Join(v, ",")
		}
		for k, v := range mux.Vars(req) {
			arguments[k] = v
		}

		params := common.HttpErrorParams{Path: req.URL.String(), Arguments: arguments}
		return entry.WithFields(logrus.Fields{
			"request_parameters": params,
			"method":             req.Method,
		})
	}

	n := negroni.New(
		negroni.NewRecovery(),
		logger,
		middleware.NewHttpCache(RequestCacheSize),
	)

	n.UseHandler(router)

	return n
}
