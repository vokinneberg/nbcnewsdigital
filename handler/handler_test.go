package handler

import (
	"NBCNewsDigital/config"
	"NBCNewsDigital/stations"
	"NBCNewsDigital/stations/api"
	"NBCNewsDigital/stations/repository"
	"NBCNewsDigital/stations/service"
	"encoding/json"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"sort"
	"testing"
)

func TestHandler_GetStations_ReturnAllStations(t *testing.T) {
	ts, teardown := startup()
	defer teardown()

	body, code := get(t, ts.URL+"/stations")
	assert.Equal(t, http.StatusOK, code)

	var data []stations.Station
	err := json.Unmarshal(body, &data)
	require.NoError(t, err)
	assert.Equal(t, true, len(data) > 20)
}

func TestHandler_GetStationsPaged_Return20Stations(t *testing.T) {
	ts, teardown := startup()
	defer teardown()

	body, code := get(t, ts.URL+"/stations?page=1")
	assert.Equal(t, http.StatusOK, code)

	var data []stations.Station
	err := json.Unmarshal(body, &data)
	require.NoError(t, err)
	assert.Equal(t, 20, len(data))

	body, code = get(t, ts.URL+"/stations?page=2")
	assert.Equal(t, http.StatusOK, code)

	data = nil
	err = json.Unmarshal(body, &data)
	require.NoError(t, err)
	assert.Equal(t, 20, len(data))
}

func TestHandler_GetStationsForNotExistingPage_ReturnEmptyArray(t *testing.T) {
	ts, teardown := startup()
	defer teardown()

	body, code := get(t, ts.URL+"/stations?page=100")
	assert.Equal(t, 200, code)

	var data []stations.Station
	err := json.Unmarshal(body, &data)
	require.NoError(t, err)
	assert.Equal(t, 0, len(data))
}

func TestHandler_GetStationsForZeroPage_ReturnAllStations(t *testing.T) {
	ts, teardown := startup()
	defer teardown()

	body, code := get(t, ts.URL+"/stations?page=0")
	assert.Equal(t, http.StatusOK, code)

	var data []stations.Station
	err := json.Unmarshal(body, &data)
	require.NoError(t, err)
	assert.Equal(t, true, len(data) > 20)
}

func TestHandler_GetStationsInService_ReturnAllInServiceStations(t *testing.T) {
	ts, teardown := startup()
	defer teardown()

	body, code := get(t, ts.URL+"/stations/in-service")
	assert.Equal(t, http.StatusOK, code)

	var data []stations.Station
	err := json.Unmarshal(body, &data)
	require.NoError(t, err)
	assert.Equal(t, true, len(data) > 20)

	sort.Slice(data, func(i int, j int) bool {
		return data[i].Id < data[j].Id
	})

	i := sort.Search(len(data), func(i int) bool {
		return data[i].StatusKey >= 3
	})

	assert.Equal(t, true, i == len(data))
}

func TestHandler_GetStationsInServicePaged_Return20OrLessInServiceStations(t *testing.T) {
	ts, teardown := startup()
	defer teardown()

	body, code := get(t, ts.URL+"/stations/in-service?page=1")
	assert.Equal(t, http.StatusOK, code)

	var data []stations.Station
	err := json.Unmarshal(body, &data)
	require.NoError(t, err)
	assert.Equal(t, 20, len(data))

	sort.Slice(data, func(i int, j int) bool {
		return data[i].Id < data[j].Id
	})

	i := sort.Search(len(data), func(i int) bool {
		return data[i].StatusKey >= 3
	})

	assert.Equal(t, true, i == len(data))
	assert.Equal(t, true, len(data) > 0 && len(data) <= 20)
}

func TestHandler_GetStationsInServiceForNotExistingPage_ReturnEmptyArray(t *testing.T) {
	ts, teardown := startup()
	defer teardown()

	body, code := get(t, ts.URL+"/stations/in-service?page=100")
	assert.Equal(t, http.StatusOK, code)

	var data []stations.Station
	err := json.Unmarshal(body, &data)
	require.NoError(t, err)
	assert.Equal(t, 0, len(data))
}

func TestHandler_GetStationsInServiceForZeroPage_ReturnAllInServiceStations(t *testing.T) {
	ts, teardown := startup()
	defer teardown()

	body, code := get(t, ts.URL+"/stations/in-service?page=0")
	assert.Equal(t, http.StatusOK, code)

	var data []stations.Station
	err := json.Unmarshal(body, &data)
	require.NoError(t, err)
	assert.Equal(t, true, len(data) > 20)

	sort.Slice(data, func(i int, j int) bool {
		return data[i].Id < data[j].Id
	})

	i := sort.Search(len(data), func(i int) bool {
		return data[i].StatusKey >= 3
	})

	assert.Equal(t, true, i == len(data))
	assert.Equal(t, true, len(data) > 0)
}

func TestHandler_GetStationsNotInServicePaged_Return20OrLessNotInServiceStations(t *testing.T) {
	ts, teardown := startup()
	defer teardown()

	body, code := get(t, ts.URL+"/stations/not-in-service")
	assert.Equal(t, http.StatusOK, code)

	var data []stations.Station
	err := json.Unmarshal(body, &data)
	require.NoError(t, err)

	sort.Slice(data, func(i int, j int) bool {
		return data[i].Id < data[j].Id
	})

	i := sort.Search(len(data), func(i int) bool {
		return data[i].StatusKey <= 1
	})

	assert.Equal(t, true, i == len(data))
	assert.Equal(t, true, len(data) > 0 && len(data) <= 20)
}

func TestHandler_GetStationsNotInServiceForZeroPage_ReturnAllNotInServiceStations(t *testing.T) {
	ts, teardown := startup()
	defer teardown()

	body, code := get(t, ts.URL+"/stations/not-in-service?page=0")
	assert.Equal(t, http.StatusOK, code)

	var data []stations.Station
	err := json.Unmarshal(body, &data)
	require.NoError(t, err)

	assert.Equal(t, true, len(data) > 0)
}

func TestHandler_GetStationsNotInServiceForNotExistingPage_ReturnEmptyArray(t *testing.T) {
	ts, teardown := startup()
	defer teardown()

	body, code := get(t, ts.URL+"/stations/not-in-service?page=100")
	assert.Equal(t, http.StatusOK, code)

	var data []stations.Station
	err := json.Unmarshal(body, &data)
	require.NoError(t, err)
	assert.Equal(t, 0, len(data))
}

func TestHandler_SearchStationsWithCorrectPattern_ReturnMatchingStations(t *testing.T) {
	ts, teardown := startup()
	defer teardown()

	body, code := get(t, ts.URL+"/stations/greene")
	assert.Equal(t, http.StatusOK, code)

	var data []stations.Station
	err := json.Unmarshal(body, &data)
	require.NoError(t, err)
	assert.Equal(t, true, len(data) > 0)
}

func TestHandler_SearchStationsWithIncorrectPattern_ReturnNotfound(t *testing.T) {
	ts, teardown := startup()
	defer teardown()

	body, code := get(t, ts.URL+"/stations/zzz")
	assert.Equal(t, http.StatusNotFound, code)

	var data struct {
		Message string `json:"message"`
	}
	err := json.Unmarshal(body, &data)
	require.NoError(t, err)
	assert.Equal(t, "not found", data.Message)
}

func TestHandler_TryToDock2BikesAtStation179_ShouldReturnTrue(t *testing.T) {
	ts, teardown := startup()
	defer teardown()

	body, code := get(t, ts.URL+"/dockable/179/2")
	assert.Equal(t, http.StatusOK, code)

	var data struct {
		Dockable bool   `json:"dockable"`
		Message  string `json:"message"`
	}
	err := json.Unmarshal(body, &data)
	require.NoError(t, err)
	assert.Equal(t, true, data.Dockable)
}

func TestHandler_TryToDock100BikesAtStation179_ShouldReturnFalse(t *testing.T) {
	ts, teardown := startup()
	defer teardown()

	body, code := get(t, ts.URL+"/dockable/179/100")
	assert.Equal(t, http.StatusOK, code)

	var data struct {
		Dockable bool   `json:"dockable"`
		Message  string `json:"message"`
	}
	err := json.Unmarshal(body, &data)
	require.NoError(t, err)
	assert.Equal(t, false, data.Dockable)
}

func startup() (ts *httptest.Server, teardown func()) {
	// Set environment variables.
	os.Setenv("NND_SERVICE_NAME", "nbc-news-digital")
	os.Setenv("NND_LOG_LEVEL", "4")
	os.Setenv("NND_DATA_SOURCE", "https://feeds.citibikenyc.com/stations/stations.json")

	// Get application config.
	conf := config.ReadConfig()

	// Init logger.
	log := logrus.New()
	log.Out = os.Stdout
	log.SetFormatter(&logrus.JSONFormatter{})
	log.SetLevel(conf.LogLevel)

	// Init dependencies.
	stationRepo := repository.NewHttpStationRepository(conf.DataSource)
	stationService := service.NewStationsService(stationRepo)
	stationHandler := api.NewStationHandler(stationService, log)

	// Init http server.
	ts = httptest.NewServer(NewHttpHandler(stationHandler, conf, log).Routes())

	teardown = func() {
		ts.Close()
	}

	return ts, teardown
}

func get(t *testing.T, url string) ([]byte, int) {
	r, err := http.Get(url)
	require.NoError(t, err)
	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)
	require.NoError(t, err)
	return body, r.StatusCode
}
