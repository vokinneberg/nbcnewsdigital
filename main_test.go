package main

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"io/ioutil"
	"math/rand"
	"net"
	"net/http"
	"os"
	"strconv"
	"syscall"
	"testing"
	"time"
)

func Test_RunMain_StartsService(t *testing.T) {
	dir, err := ioutil.TempDir(os.TempDir(), "nbc-news-digital")
	require.NoError(t, err)
	defer os.RemoveAll(dir)

	port := chooseRandomUnusedPort()
	os.Setenv("NND_SERVICE_NAME", "nbc-news-digital")
	os.Setenv("NND_SERVICE_HOST", "127.0.0.1")
	os.Setenv("NND_SERVICE_PORT", strconv.Itoa(port))
	os.Setenv("NND_LOG_LEVEL", "4")
	os.Setenv("NND_DATA_SOURCE", "https://feeds.citibikenyc.com/stations/stations.json")

	done := make(chan struct{})
	go func() {
		<-done
		err := syscall.Kill(syscall.Getpid(), syscall.SIGTERM)
		require.NoError(t, err)
	}()

	finished := make(chan struct{})
	go func() {
		main()
		close(finished)
	}()

	defer func() {
		close(done)
		<-finished
	}()

	waitForHTTPServerStart(port)
	resp, err := http.Get(fmt.Sprintf("http://localhost:%d/health", port))
	require.NoError(t, err)
	defer resp.Body.Close()
	assert.Equal(t, http.StatusOK, resp.StatusCode)
}

func chooseRandomUnusedPort() (port int) {
	for i := 0; i < 10; i++ {
		port = 40000 + int(rand.Int31n(10000))
		if ln, err := net.Listen("tcp", fmt.Sprintf(":%d", port)); err == nil {
			_ = ln.Close()
			break
		}
	}
	return port
}

// Wait for up to 10 seconds for server to start before returning it.
func waitForHTTPServerStart(port int) {
	client := http.Client{Timeout: time.Second}
	for i := 0; i < 100; i++ {
		time.Sleep(time.Millisecond * 100)
		if resp, err := client.Get(fmt.Sprintf("http://localhost:%d", port)); err == nil {
			_ = resp.Body.Close()
			return
		}
	}
}
