BINARY_NAME := nbc-news-digital
BINARY_OUTPUT_PATH := ./bin/$(BINARY_NAME)
SHELL := /bin/bash
PLATFORM := $(shell go env GOOS)
ARCH := $(shell go env GOARCH)
GOPATH := $(shell go env GOPATH)
GOBIN := $(GOPATH)/bin
GOCMD := go
GOFLAGS := -mod=vendor

default: clean test build

lint:
	 golangci-lint run ./... --skip-dirs-use-default --exclude=SA1019

clean:
	rm -rf ./bin

build:
	go fmt ./...
	DEP_BUILD_PLATFORMS=$(PLATFORM) DEP_BUILD_ARCHS=$(ARCH) $(GOCMD) build -o $(BINARY_OUTPUT_PATH) -v $(GOFLAGS)

test:
	$(GOCMD) test -v -race ./...

install: build
	cp ./bin $(GOBIN)

.PHONY: build test clean install lint